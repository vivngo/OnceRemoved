#!/bin/bash
# A script to make a directory for each photo
for filename in images/fulls/*.jpg; do
	basename="$(basename "$filename" .jpg)"
	mkdir images/fulls/$basename
	mv images/fulls/$basename.jpg images/fulls/$basename
done
