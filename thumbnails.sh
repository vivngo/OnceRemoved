#!/bin/bash
# A script to make thumbnails for all images in full/ folder
for filename in images/fulls/*.jpg; do
	basename="$(basename "$filename" .jpg)"
	convert "images/fulls/$basename.jpg" -thumbnail 1024x512 "images/thumbs/$basename.jpg"
done
