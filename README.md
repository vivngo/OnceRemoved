# Once Removed
A project that demonstrates how context can change the meaning of art.

## Image Directory Structure
Each art piece will have a subfolder under `images/fulls` and a thumbnail under `images/thumbs`. In particular, the `images` directory will look like this:

```sh
images
├── fulls
|   ├── ArtPiece1
|   |   ├ ArtPiece1.jpg				# full size image of 1st art piece in original setting
|   |   └ ArtPiece1_SettingA.jpg	# full size image of 1st art piece in an alternate setting
|   ├── ArtPiece2
|   |   ├ ArtPiece2.jpg				# full size image of 2nd art piece in original setting
|   |   ├ ArtPiece2_SettingB.jpg	# full size image of 2nd art piece in an alternate setting
|   |   └ ArtPiece2_SettingC.jpg	# NOTE: We can have more than one alternate settings per art piece
|   └── ArtPiece3
|       ├ ArtPiece3.jpg				# full size image of 3rd art piece in original setting
|       └ ArtPiece3_SettingD.jpg	# full size image of 3rd art piece in an alternate setting
└── _thumbs
    ├── ArtPiece1.jpg
    ├── ArtPiece2.jpg
    └── ArtPiece2.jpg
```

## Credits
Thanks to [Ram Swaroop](https://github.com/ramswaroop) for his [Photography](https://github.com/ramswaroop/photography) jekyll template.
